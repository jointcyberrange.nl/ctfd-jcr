# Plugin Structure

```python
CTFd
└── plugins
    └── container_challenges
        └── assets
            └── create.html
            └── create.js
            └── update.html
            └── update.js
            └── view.html
            └── view.js
        └── code
            └── kubernetes.py
            └── models.py
            └── routes.py
        └── templates
            └── configuration.html
            └── namespaces.html
        └── __init__.py
```

## `create.html`

This file represents the custom creation page for container challenges. Admins can create challenges here.

## `create.js`

This file is not important.

## `update.html`

This file represents the custom modification page for container challenges. Admins can update challenges here.

## `update.js`

This file is not important.

## `view.html`

This file represents the custom preview page for container challenges. Users can try to solve challenges here.

## `view.js`

This file operates the plugin backend based on the user's actions.

## `configuration.html`

This file represents the plugin configuration page. Admins can configure the plugin here.

## `namespaces.html`

This file represents the Kubernetes namespace management page. Admins can see/delete Kubernetes namespaces here.

## `__init__.py`

This file is the CTFd plugin entrypoint that is called first when CTFd starts. It is the core file of the plugin.

## `routes.py`

This file contains all Flask routes.

## `kubernetes.py`

This file contains all Kubernetes functionalities.

## `models.py`

This file contains all database models needed for this plugin.
